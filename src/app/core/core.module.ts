import { AuthService } from './auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  providers: [AuthService],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
